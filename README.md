# AlphaGrid Interactive - Santander Evs #


### Dev ###

npm install

npm run fetch to pull latest content (see notes below)

npm start for development

npm run build for final output


### To fetch the data

Using Dropbox Paper to manage this project.

You need an access token https://dropbox.tech/developers/generate-an-access-token-for-your-own-account

and the document to be shared with your account.

Document is located https://paper.dropbox.com/doc/Santander-EVs--BJNTgRQZQdhSNVw5r4vgh133Ag-BS6VOjf3TsUSDCrkBCrDL

Last step is to set up a `.env` file at the root of the project that has your access token. It should look like

`DROPBOX_ACCESS_TOKEN=XXXXXXXXXXXXXXXX`

Once you have access token in the .env file and the document has been shared with you, run `npm run fetch`

This will save a copy of the data to `src/data/data.json`

When you pull new copy with the command line "npm run fetch", please stop webpack and run $ npm start again.

To switch from english to spanish, edit the line 12 of interactive.hsb and reload the page.
{{#each data.lang_es }} 

or

{{#each data.lang_en }}

The spanish version has an extra CSS file to fix all little CSS tweak related to font sizes. (located in styles > components > _lang_es.scss) 

That's all.

### Live url ###
https://santanderprivatebanking.ft.com/the-road-to-a-fully-electric-future


### Notes ###
By AlphaGrid - developer Denis Bouquet (freelance)
