# Santander EVs

Work in progress:
**English** https://www.alphagridinfographics.com/santander-evs/
**Spanish** https://www.alphagridinfographics.com/santander-evs/es/

Test page within the TVN hub with FT header and footer: 
**English** https://ft-santander-pb.trunky.net/Full-Page-Infographic
**Spanish** (HTML not share with TVN yet but soon)

[.lang_en]

assets-root: https://www.alphagridinfographics.com/santander-evs/
language: en

page-title: 
page-description:
social-media-share-copy: 
twitter-copy: 


## Section 1 - The road to a fully electric future

s1-title: The road to a fully electric future

s1-p1: <p>Sales of electric cars have been accelerating steadily for more than a decade, topping a record 3.2 million globally in 2020 even as the Covid-19 pandemic slowed overall sales of passenger cars.</p>

s1-ctascroll: scroll

s1-p2: <p>Tax incentives, stricter fuel-economy standards, eco-conscious consumers and new, lower-priced models are expected to rev up sales even further this year — by as much as 70 per cent.</p><p>But what does this mean for the electric vehicle (EV) market and the businesses that power it?</p><p>Join us on an interactive road trip through the data to explore the countries that are leading the charge, the competitors catching up with Tesla, the winners and losers along the supply chain, and then finish off with an EV race around the planet!</p>

s1-2-title: Growth of the EV market

queen-ev-caption: 1906 Wood's Queen Victoria Electric Car

tesla-modelx-caption: Tesla Model X

s1-2-p1: <p>The electric car made its first appearance in the late 1800s in the form of light battery-powered carriages. While they soon outsold more unwieldy vehicles powered by horses, steam and gasoline, their popularity didn’t last.</p><p>The advent of the internal-combustion engine (ICE) in the early 1900s made it cheaper to run a car on petrol, which was more widely available and powered vehicles for longer distances – pushing EVs off the road for decades</p>

s1-2-p2: <p>Now, over a century later, that trend is on course to go into reverse.</p><p>In 2020, EVs accounted for more than 4 per cent of all new vehicle sales worldwide, almost double its market share a year earlier.</p>

s1-2-p3: <p>While the market cap for EVs has since pulled back slightly, it remains neck-and-neck with ICE vehicles.</p><p>The switch to EVs will continue at pace as governments introduce tough emissions policies and generous incentives to go electric. Global sales are forecast to grow by 29 per cent a year over the next decade, exceeding 31 million by 2030.</p>


![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619613312889_s1-graph1-mobile.png)
![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619611961153_s1-graph1-desktop.png)


s1-graph1-title: Global plug-in vehicle markets

s1-graph1-label1:China
s1-graph1-label2:Germany
s1-graph1-label3:US
s1-graph1-label4:France
s1-graph1-label5:UK
s1-graph1-label6:Norway
s1-graph1-label7:Sweden
s1-graph1-label8:Italy
s1-graph1-label9:Netherlands
s1-graph1-label10:Other
s1-graph1-label11:South Korea
s1-graph1-label12:Belgium
s1-graph1-label13:Canada
s1-graph1-label14: +43%
s1-graph1-label15: growth
s1-graph1-label-k: k

s1-graph1-source: Source: EV Volumes


![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619613338112_s1-graph2-de.png)
![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619611968837_s1-graph2-desktop.png)


s1-graph2-title: Electric vehicles market cap versus legacy automobiles (USD bn)

s1-graph2-legend1: Legacy Automobile Stocks*
s1-graph2-legend2: Electric Vehicle Stocks**

s1-graph2-line1: 23 November 2020
s1-graph2-line2: The capitalization of EV stocks
s1-graph2-line3: exceeds the combined market
s1-graph2-line4: cap of traditional auto companies

s1-graph2-footnotes: *Traditional auto stocks = Toyota, VW, BMW, Daimler, GM, Stellantis, Ferrari, Honda, Hyundai, Ford, Suzuki, Maruti<br/>**EV stocks = Tesla, NIO, Byd, Evergrande, NV, Xpeng, Nikola, Green Power, Arcimoto, Kandi, Workhorse  

s1-graph2-source: Sources: Bloomberg; Santander




## Section 2 - Countries and regions leading the charge

s2-title: Countries and regions leading the charge

s2-p1: <p>China is the world’s largest EV market by far, followed by Europe and the US. Together, they account for 94 per cent of global EV sales.</p><p>On a per capita basis, Norway takes the prize for sales. Last year, it became the first country in the world where electric car sales were higher than the combined sales of cars powered by petrol, diesel and hybrid engines.</p><p>But even in countries with comparatively lower EV sales, uptake is growing exponentially. In Spain, for example, EV registrations topped 20,000 in 2020, a 64 per cent jump on the year before.</p><p>China and Europe will continue to dominate the EV market, driven by tough regulations and generous incentives.</p>

pin-usa: The US was the third-largest market for plug-in electric passenger car sales in 2020 with 328,000 sales, up 4 per cent on 2019.

pin-spain: The Spanish government allocated €550m for buyers who replace cars that are more than 10 years old with an A or B energy-label vehicle, emitting less than 120g CO2/km, regardless of fuel type.

pin-china: Beijing has imposed a mandate on car manufacturers to ensure electric vehicles make up 40 per cent of their total sales by 2030.

pin-chile: Chile has the largest reserves of lithium.

pin-norway: Norway leads the sale of EVs as a percentage of total sales.

pin-southkorea: South Korea's LG Chem is the world’s biggest maker of lithium-ion batteries after expanding cell capacity. 

s2-footnote: *Benchmark Mineral Intelligence
s2-caption: Sources: ACEA; EM-Volumes; JPMorgan


## Section 3 - Carmakers in the fast lane

s3-title: Carmakers in the fast lane

s3-p1: <p>In the race to go electric, Tesla has long held pole position — and not just for EVs. Its market cap soared beyond $800bn in January, putting its worth on a par with the world’s nine largest carmakers combined.</p><p>The US car pioneer will need to continue innovating to keep its lead as competitors vie for a bigger slice of the market.</p><p>Carmakers new and old are set to unveil dozens of new EVs this year, with 520 new models expected to hit the road by 2022.</p>

s3-ctascroll: Keep scrolling

s3-chart-title: Global plug-in electric vehicle market share in 2020, by main producer (%)

s3-chart-symbol: %
s3-chart-label-tesla: Tesla
s3-chart-value-tesla: 16
s3-chart-valuegraph-tesla: 100
s3-chart-label-vw-group: VW Group
s3-chart-value-vw-group: 13
s3-chart-valuegraph-vw-group: 81
s3-chart-label-saic: SAIC
s3-chart-value-saic: 9
s3-chart-valuegraph-saic: 56
s3-chart-label-renault–nissan:Renault–Nissan–Mitsubishi Alliance
s3-chart-value-renault–nissan: 7
s3-chart-valuegraph-renault–nissan: 44
s3-chart-label-bmw-group: BMW Group
s3-chart-value-bmw-group: 6
s3-chart-valuegraph-bmw-group: 38

s3-marketshare-caption: Source: Statista



## Section 4 - Plugging in: The complex EV battery supply chain

s4-title: Plugging in: The complex EV battery supply chain

s4-p1: <p>Manufacturing an electric vehicle requires fewer mechanical parts than for a traditional car but involves many more electric and electronic components. With carmakers set to roll out more than 30 million EVs a year by 2030, demand for materials risks exceeding supply.</p><p>The biggest challenge facing carmakers lies in sourcing the raw materials that make up the thousands of cells in each EV battery, the most expensive part of an EV.</p><p>Scroll over the car battery to find out which materials will be most in demand as production ramps up.</p>

battery-layer-lithium:<strong>Lithium</strong> The most important material in battery manufacturing. Global demand for lithium is expected to grow at an annual rate of 25.5 per cent a year until 2024. Falling prices in the last three years slowed investment in lithium mining, leading to supply shortage. Prices only started recovering this year as government incentives boosted demand. Lithium miners and researchers are looking to develop innovative ways to produce the material more efficiently.

battery-layer-cobalt:<strong>Cobalt</strong> About 60 per cent of the world’s supply is in the Democratic Republic of Congo, which is notorious for dangerous and unregulated mining practices. China controls most of the exports.

battery-layer-nickel:<strong>Nickel</strong> Global demand for nickel is expected to jump to some 665,000 tonnes globally by 2025. There is no shortage of the metal but only a few countries, such as China, have the capacity to refine it.

battery-layer-graphite:<strong>Graphite</strong> Used in the battery anodes of many EVs. Nearly two-thirds of natural graphite is mined in China.

battery-layer-nanotechnology: <strong>Nanotechnology</strong> Can be applied to everything from batteries to body parts to paint coatings. Batteries made from nanotech materials are lighter, which helps to prolong their lifespan.
 
battery-caption: Sources: Car Magazine; Trading Economics<br/>Image credits: Shutterstock


## Section 5 - Who has the power?

s5-title: Who has the power?

s5-p1: <p>Asian manufacturers dominate the global EV battery market. South Korea’s LG Chem overtook China’s CATL and Japan’s Panasonic as the biggest maker after hiving off its EV battery business late last year.</p>

s5-p2: <p>Even so, China remains the biggest maker of lithium-ion batteries, the most common type of battery used to power EVs.</p><p>Europe is forecast to become the second largest producer as it boosts investment to meet demand from regional carmakers, which must expand their EV line-up to avoid emission penalties for new cars.</p>

s5-chart-title: Top 5 lithium-ion battery producers by capacity (GWh per year)

s5-chart-caption: Source: Benchmark Mineral Intelligence

s5-battery-caption: Close-up of EV car battery 

s5-pie-title: Lithium-ion battery capacity share*<br/>(%)

s5-pie-year-2020: 2020
s5-pie-year-2020-value-china-label: China
s5-pie-year-2020-value-china-value: 77%
s5-pie-year-2020-value-asia-label1: Asia
s5-pie-year-2020-value-asia-label2: ex China 
s5-pie-year-2020-value-asia-value: 8%
s5-pie-year-2020-value-europe-label: Europe 
s5-pie-year-2020-value-europe-value: 6%
s5-pie-year-2020-value-us-label: US 
s5-pie-year-2020-value-us-value: 9%
s5-pie-year-2020-value-australasia-label: Australasia 
s5-pie-year-2020-value-australasia-value: 0% 

s5-pie-year-2025: 2025
s5-pie-year-2025-value-china-label: China
s5-pie-year-2025-value-china-value: 65%
s5-pie-year-2025-value-asia-label1: Asia 
s5-pie-year-2025-value-asia-label2: ex China
s5-pie-year-2025-value-asia-value: 3%
s5-pie-year-2025-value-europe-label: Europe 
s5-pie-year-2025-value-europe-value: 25%
s5-pie-year-2025-value-us-label: US 
s5-pie-year-2025-value-us-value: 6%
s5-pie-year-2025-value-australasia-label: Australasia 
s5-pie-year-2025-value-australasia-value: 1% 


s5-pie-caption: *As of February 1, 2021 <br/>Source: S&P Global Market Intelligence


## Section 6 - Taking over the road

s6-title: Taking over the road

s6-p1: <p>With countries and manufacturers throughout the supply chain shifting to more sustainable transport, when can we expect EVs to take over the road? Consumers will only make the switch in large numbers when EVs become as affordable to run as petrol cars.</p><p>Battery prices have already fallen dramatically this decade, but they will need to drop to around $100/kWh for EVs to become as cost-competitive as ICE vehicles — a tipping point that’s expected to be reached in the middle of this decade.</p>

s6-chart-title: Volume weighted average price of battery packs for electric vehicles <br />($ per kWh)*

s6-chart-label-2010: 2010
s6-chart-value-2010: 1160
s6-chart-label-2011: 2011
s6-chart-value-2011: 899
s6-chart-label-2012: 2012
s6-chart-value-2012: 707
s6-chart-label-2013: 2013
s6-chart-value-2013: 650
s6-chart-label-2014: 2014
s6-chart-value-2014: 577
s6-chart-label-2015: 2015
s6-chart-value-2015: 373
s6-chart-label-2016: 2016
s6-chart-value-2016: 288
s6-chart-label-2017: 2017
s6-chart-value-2017: 214
s6-chart-label-2018: 2018
s6-chart-value-2018: 176
s6-chart-label-2019: 2019
s6-chart-value-2019: 156
s6-data-separator:

s6-chart-label-2024: 2024
s6-chart-value-2024: 100


s6-chart-caption: *Average prices weighted based on volume sold, the battery pack used in Tesla Model 3 range from 50 to 70 kWh <br/>Source: BloombergNEF


## Section 7 - Electric odyssey

s7-title: Electric odyssey

s7-p1: <p>EV ownership is set to receive a turbo boost this year as motorsport fans tune in to Extreme-E. The world’s first electric off-road racing series, which kicked off in April, features electric SUVs competing in extreme environments around the world to raise awareness of climate change and accelerate EV adoption.</p>

locations-title: Race locations, 2021: Remote areas damaged by human activity

location-1: <span>Alula</span> Location: Saudi Arabia<br />Terrain: Desert
location-1-date: April 3-4
location-2: <span>Lac Rose</span> Location: Dakar, Senegal<br />Terrain: Beach
location-2-date: May 29-30
location-3: <span>Kangerlussaq</span> Location: Greenland<br />Terrain: Arctic
location-3-date: August 28-29
location-4: <span>Pará</span>Location: Santarém, Brazil<br />Terrain: Rainforest
location-4-date: October 23-24
location-5: <span>Tierra Del Fuego</span> Location: Argentina<br />Terrain: Glacier
location-5-date: December 11-12


s7-p2: <p>The competition vehicle, designed to deal with the most hostile terrains, pushes the boundaries of EV range and power.</p>

s7-photo-title: ACCIONA SAINZ XE Team car

s7-photo-caption: Source: ACCIONA SAINZ XE Team

s7-p3: <p>As motor enthusiasts follow the race, carmakers will be assessing how these high-tech cars perform and will apply what they learn to the next generation of EVs.</p>


## Section 8 - conclusion

s8-title: Into the future…

s8-p: <p>This will take the electric car to places that the pioneers of battery-operated vehicles could never have dreamt of — and will further fuel demand for a clean form of transport whose time has finally come.</p><p>EV sales are forecast to grow by 29 per cent a year over the next decade, exceeding 31 million by 2030. By 2040, well over half of all passenger vehicles sold will be electric.</p>

s8-counter: EVs 50% of all car sales

[]



----------


# **Spanish version** 

****
[.lang_es]
assets-root: https://www.alphagridinfographics.com/santander-evs/
language: es

page-title: 
page-description:
social-media-share-copy: 
twitter-copy: 


## Sección 1 - The road to a fully electric future - SPANISH

s1-title: El camino hacia un futuro totalmente eléctrico

s1-p1: <p>La venta de automóviles eléctricos se ha incrementado constantemente desde hace más de una década: en 2020, superó el récord mundial y llegó a los 3,2 millones incluso cuando, debido a la pandemia de la COVID-19, hubo un descenso generalizado en las ventas de turismos.</p>

s1-ctascroll: desplazarse

s1-p2: <p>Se espera que este año, dicho volumen siga incrementando, hasta en un 70%, gracias a los incentivos fiscales, a unas pautas de ahorro de combustible más estrictas, a los consumidores concienciados con la ecología, y a la caída del precio de los modelos nuevos.</p><p>Pero, ¿qué significa esto para el mercado de vehículos eléctricos (VE) y las empresas que lo impulsan?</p><p>Únete a nosotros en un viaje por carretera interactivo, a través de los datos, para así conocer cuales son los países que lideran esta tendencia, la competencia que trata de ponerse al nivel de Tesla, y a los ganadores y perdedores a lo largo de la cadena de suministro. ¡Y terminaremos con una carrera de VE alrededor del planeta!</p>

s1-2-title: Crecimiento del mercado de vehículos eléctricos

queen-ev-caption: 1906 Coche eléctrico Woods Queen Victoria

tesla-modelx-caption: Modelo X de Tesla

s1-2-p1: <p>El automóvil eléctrico hizo su primera aparición a finales del siglo XIX en forma de vehículos ligeros alimentados por baterías. Si bien superó rápidamente en ventas a otros vehículos menos manejables, como los arrastrados por caballos o los propulsados por vapor y gasolina, su popularidad no perduró.</p><p>La llegada del motor de combustión interna (ICE) a principios del siglo XX hizo que fuera más económico el funcionamiento de los coches con gasolina, cuya disponibilidad era mucho mayor y permitía recorrer distancias más largas; esto derivó en que los vehículos eléctricos se alejaron de las carreteras durante décadas.</p>

s1-2-p2: <p>Ahora, más de un siglo después, esa tendencia está en vías de invertirse.</p><p>En 2020, los VE representaron más del 4% de todas las ventas de vehículos nuevos en el mundo; casi el doble de su cuota de mercado un año antes.</p>

s1-2-p3: <p>Una tendencia que, aun a pesar de que la capitalización de mercado de los vehículos eléctricos ha retrocedido ligeramente desde entonces, sigue estando a la par de los vehículos de combustión interna.</p><p>El cambio a los vehículos eléctricos seguirá avanzando a medida que los gobiernos introduzcan políticas estrictas en materia de emisiones y generosos incentivos para apostar por la electricidad. Se prevé que las ventas mundiales crezcan un 29% al año durante la próxima década y que superen los 31 millones en 2030.</p>


![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619613312889_s1-graph1-mobile.png)
![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619611961153_s1-graph1-desktop.png)


s1-graph1-title: Mercados globales de vehículos enchufables

s1-graph1-label1:China
s1-graph1-label2:Alemania
s1-graph1-label3:EE. UU.
s1-graph1-label4:Francia
s1-graph1-label5:Reino Unido
s1-graph1-label6:Noruega
s1-graph1-label7:Suecia
s1-graph1-label8:Italia
s1-graph1-label9:Holanda
s1-graph1-label10:Otros
s1-graph1-label11:Corea del Sur
s1-graph1-label12:Bélgica
s1-graph1-label13:Canadá
s1-graph1-label14: +43%
s1-graph1-label15:
s1-graph1-label-k: m

s1-graph1-source: Fuente: Volúmenes de VE


![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619613338112_s1-graph2-de.png)
![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1619611968837_s1-graph2-desktop.png)


s1-graph2-title: Capitalización de mercado de vehículos eléctricos frente a automóviles tradicionales (en miles de millones de USD)

s1-graph2-legend1: Stocks de automóviles tradicionales*
s1-graph2-legend2: Stocks de vehículos eléctricos**

s1-graph2-line1: 23 de noviembre de 2020
s1-graph2-line2: La capitalización de stocks de VE
s1-graph2-line3: supera la capitalización de mercado 
s1-graph2-line4:  combinada de las empresa 
s1-graph2-line5: automovilísticas tradicionales

s1-graph2-footnotes: *Stocks de automóviles tradicionales = Toyota, VW, BMW, Daimler, GM, Stellantis, Ferrari, Honda, Hyundai, Ford, Suzuki y Maruti<br/>**Stocks de VE = Tesla, NIO, Byd, Evergrande, NV, Xpeng, Nikola, GreenPower, Arcimoto, Kandi y Workhorse  

s1-graph2-source: Fuentes: Bloomberg; Santander




## Sección 2 - Countries and regions leading the charge - SPANISH

s2-title: Países y regiones que lideran la tendencia

s2-p1: <p>China es, con diferencia, el mayor mercado de VE del mundo, seguido por Europa y los Estado Unidos. Juntos, representan el 94% de las ventas mundiales de VE.</p><p>Sin embargo, es Noruega quien se lleva la palma en ventas per cápita, convirtiéndose el año pasado en el primer país del mundo en el que las ventas de automóviles eléctricos superaron a la venta en conjunto de automóviles  propulsados por motores de gasolina, diésel e híbridos.</p><p>Aun así, en aquellos países donde las ventas de VE son comparativamente bajas, la aceptación está creciendo exponencialmente. Las matriculaciones de VE en España, por ejemplo, superaron las 20.000 en 2020; esto representa un incremento del 64% respecto al año anterior.</p><p>China y Europa continuarán dominando el mercado de VE, impulsados por normativas estrictas y generosos incentivos.</p>

pin-usa: EE. UU., durante 2020, fue el tercer mayor mercado en ventas de turismos eléctricos enchufables con 328.000 unidades vendidas, lo que supone un 4% más que en 2019.

pin-spain: El Gobierno español destinó 550 millones de euros a ayudas a compradores que cambiaran sus coches de más de 10 años por un vehículo de etiqueta energética A o B, que emite menos de 120 g de CO2/km, independientemente del tipo de combustible.

pin-china: Pekín ha establecido la obligación de que los fabricantes de automóviles garanticen que los vehículos eléctricos representen el 40% de sus ventas totales en 2030.

pin-chile: Chile cuenta con las mayores reservas de litio.

pin-norway: Noruega lidera la venta de vehículos eléctricos con respecto a las ventas totales.

pin-southkorea: LG Chem es el mayor fabricante mundial de baterías de iones de litio tras ampliar la capacidad de las celdas.

s2-footnote: *Benchmark Mineral Intelligence
s2-caption: Fuentes: ACEA; EM-Volumes; JPMorgan


## Sección 3 - Carmakers in the fast lane - SPANISH

s3-title: Fabricantes de automóviles en la vía rápida

s3-p1: <p>Tesla ha mantenido durante mucho tiempo la pole position en la carrera de lo eléctrico, y no solo en lo que respecta a los vehículos. Su capitalización bursátil superó en enero los 800.000 millones de dólares; su valor se equipara al de los nueve mayores fabricantes de automóviles del mundo juntos.</p><p>El pionero automovilístico estadounidense deberá seguir innovando si quiere mantener su liderazgo mientras la competencia compite por hacerse con una mayor cuota de mercado.</p><p>Los fabricantes de automóviles nuevos y viejos están listos para presentar docenas de vehículos eléctricos nuevos este año; se espera que para 2022 salgan a la carretera 520 modelos nuevos.</p>

s3-ctascroll: seguir desplazándose

s3-chart-title: Cuota de mercado global de vehículos eléctricos enchufables en 2020, por productor principal

s3-chart-symbol: %
s3-chart-label-tesla: Tesla
s3-chart-value-tesla: 16
s3-chart-valuegraph-tesla: 100
s3-chart-label-vw-group: Grupo VW
s3-chart-value-vw-group: 13
s3-chart-valuegraph-vw-group: 81
s3-chart-label-saic: SAIC
s3-chart-value-saic: 9
s3-chart-valuegraph-saic: 56
s3-chart-label-renault–nissan:Alianza Renault – Nissan – Mitsubishi
s3-chart-value-renault–nissan: 7
s3-chart-valuegraph-renault–nissan: 44
s3-chart-label-bmw-group: BMW Group
s3-chart-value-bmw-group: 6
s3-chart-valuegraph-bmw-group: 38

s3-marketshare-caption: Fuente: Statista



## Sección 4 - Plugging in: The complex EV battery supply chain - SPANISH

s4-title: Conectando: La compleja cadena de suministro de baterías para VE

s4-p1: <p>La fabricación de un vehículo eléctrico requiere menos piezas mecánicas que la de uno tradicional; a la vez, conlleva el uso de muchos más componentes eléctricos y electrónicos. Se corre el riesgo de que la demanda de materiales supere a la oferta, pues los fabricantes de automóviles prevén la fabricación de más de 30 millones de vehículos eléctricos al año de aquí a 2030.</p><p>El mayor desafío al que se enfrentan los fabricantes de automóviles radica en obtener las materias primas que componen las miles de celdas de cada batería de un VE, la parte más cara de dichos vehículos.</p><p>Descubre qué materiales serán los más demandados a medida que aumente la producción, pasando el cursor por la batería del coche.</p>

battery-layer-lithium:<strong>Litio</strong> el material más importante en la fabricación de baterías. Se prevé que, hasta 2024, la demanda mundial de litio crezca a una tasa anual del 25,5%. La caída de los precios en los últimos tres años ha desacelerado la inversión en la extracción de litio, lo que ha derivado en una escasez de oferta. Los precios no han empezado a recuperarse hasta este año, pues los incentivos gubernamentales han impulsado la demanda. Los mineros e investigadores del litio pretenden desarrollar formas innovadoras de producir el material de manera más eficiente.

battery-layer-cobalt:<strong>Cobalto</strong> en la República Democrática del Congo, conocida por sus prácticas mineras peligrosas y no reguladas, se halla entorno al 60% de reservas mundiales. China controla la mayoría de las exportaciones.

battery-layer-nickel:<strong>Níquel</strong> se prevé que la demanda mundial de níquel aumente a unas 665.000 toneladas en todo el mundo para 2025. Aun sin escasez de este metal, son pocos los países que cuentan con la capacidad de refinarlo, como es el caso de China.

battery-layer-graphite:<strong>Grafito</strong> se utiliza en los ánodos de la batería de muchos vehículos eléctricos. Casi dos tercios del grafito natural se extrae en China.

battery-layer-nanotechnology: <strong>Nanotecnología</strong> puede aplicarse a todo, desde las baterías hasta las piezas de carrocería o los revestimientos de pintura. Las baterías fabricadas con materiales nanotecnológicos son más ligeras, lo que contribuye a prolongar su vida útil.
 

## Sección 5 - Who has the power? - SPANISH

s5-title: ¿Quién controla la producción de baterías?

s5-p1: <p>Los fabricantes asiáticos dominan el mercado mundial de baterías para VE. La surcoreana LG Chem superó a la china CATL y a la japonesa Panasonic como mayor fabricante, tras independizar su negocio de baterías para VE a finales del año pasado.</p>

s5-p2: <p>Aun así, China sigue siendo el mayor fabricante de baterías de iones de litio, el tipo de batería más utilizado para alimentar los vehículos eléctricos.</p><p>Se prevé que Europa se convierta en el segundo mayor productor; está impulsando la inversión para satisfacer la demanda de los fabricantes regionales de automóviles, que deben ampliar su gama de VE para evitar las sanciones por emisiones de los coches nuevos.</p>

s5-chart-title: Los cinco principales productores de baterías de iones de litio por capacidad (GWh al año)

s5-chart-caption: Fuente: Benchmark Mineral Intelligence

s5-battery-caption: Primer plano de la batería del VE

s5-pie-title: Cuota de mercado de baterías de iones de litio*<br/>(%)

s5-pie-year-2020: 2020
s5-pie-year-2020-value-china-label: China
s5-pie-year-2020-value-china-value: 77%
s5-pie-year-2020-value-asia-label1: Asia
s5-pie-year-2020-value-asia-label2:ex China  
s5-pie-year-2020-value-asia-value: 8%
s5-pie-year-2020-value-europe-label: Europa 
s5-pie-year-2020-value-europe-value: 6%
s5-pie-year-2020-value-us-label: EE. UU. 
s5-pie-year-2020-value-us-value: 9%
s5-pie-year-2020-value-australasia-label: Australasia 
s5-pie-year-2020-value-australasia-value: 0% 

s5-pie-year-2025: 2025
s5-pie-year-2025-value-china-label: China
s5-pie-year-2025-value-china-value: 65%
s5-pie-year-2025-value-asia-label1: Asia
s5-pie-year-2025-value-asia-label2: ex China 
s5-pie-year-2025-value-asia-value: 3%
s5-pie-year-2025-value-europe-label: Europa
s5-pie-year-2025-value-europe-value: 25%
s5-pie-year-2025-value-us-label: EE. UU. 
s5-pie-year-2025-value-us-value: 6%
s5-pie-year-2025-value-australasia-label: Australasia 
s5-pie-year-2025-value-australasia-value: 1% 


s5-pie-caption: *A 1 de febrero de 2021<br/>Fuente: S&P Global Market Intelligence


## Sección 6 - Taking over the road - SPANISH

s6-title: Tomando el control de la carretera

s6-p1: <p>¿Cuándo veremos que se imponga el uso de vehículos eléctricos en las carreteras, ahora que los países y los fabricantes de toda la cadena de suministro están adoptando un sistema de transporte más sostenible? El cambio a gran escala por parte de los consumidores solo será posible cuando los vehículos eléctricos sean tan asequibles como los de gasolina.</p><p>En esta década se ha producido una drástica reducción en el precio de las baterías. Sin embargo, para que los vehículos eléctricos sean tan competitivos en cuanto a costes como los de motor de combustión interna, tendrán que bajar a unos 100 dólares por kilovatio, un punto de inflexión que se espera que se alcance a mediados de esta década.</p>

s6-chart-title: Precio medio ponderado por volumen de los paquetes de baterías para vehículos eléctricos<br />(USD por kWh)*

s6-chart-label-2010: 2010
s6-chart-value-2010: 1.160
s6-chart-label-2011: 2011
s6-chart-value-2011: 899
s6-chart-label-2012: 2012
s6-chart-value-2012: 707
s6-chart-label-2013: 2013
s6-chart-value-2013: 650
s6-chart-label-2014: 2014
s6-chart-value-2014: 577
s6-chart-label-2015: 2015
s6-chart-value-2015: 373
s6-chart-label-2016: 2016
s6-chart-value-2016: 288
s6-chart-label-2017: 2017
s6-chart-value-2017: 214
s6-chart-label-2018: 2018
s6-chart-value-2018: 176
s6-chart-label-2019: 2019
s6-chart-value-2019: 156
s6-data-separator:.

s6-chart-label-2024: 2024
s6-chart-value-2024: 100


s6-chart-caption: *Precio medio ponderado según el volumen vendido; el paquete de baterías utilizado en el Tesla Model 3 varía de 50 a 70 kWh <br/>Fuente: BloombergNEF


## Sección 7 - Electric odyssey - SPANISH

s7-title: Odisea eléctrica

s7-p1: <p>Los vehículos eléctricos recibirán un gran impulso este año a medida que los aficionados al automovilismo se interesen por las competiciones de Extreme-E. Comenzando en abril, la primera serie de carreras de todoterrenos eléctricos tendrá lugar en entornos extremos de todo el mundo, con el fin de concienciar sobre el cambio climático y acelerar la adopción de los vehículos eléctricos.</p>

locations-title: Localizaciones de la carrera, 2021: Áreas remotas dañadas por la actividad humana

location-1: <span>Alula</span> Localización: Arabia Saudí<br />Terreno: Desierto
location-1-date: 3-4 de abril
location-2: <span>Lac Rose</span> Localización: Dakar, Senegal<br />Terreno: Playa
location-2-date: 29-30 de mayo
location-3: <span>Kangerlussaq</span> Localización: Groenlandia<br />Terreno: Ártico
location-3-date: 28-29 de agosto
location-4: <span>Pará</span>Localización: Santarém, Brasil<br />Terreno: Selva
location-4-date: 23-24 de octubre
location-5: <span>Tierra Del Fuego</span> Localización: Argentina<br />Terreno: Glaciar
location-5-date: 11-12 de diciembre


s7-p2: <p>El vehículo de competición, diseñado para enfrentarse a los terrenos más hostiles, supera los límites de autonomía y potencia de los VE convencionales.</p>

s7-photo-title: ACCIONA SAINZ XE Team car

s7-photo-caption: Fuente: ACCIONA SAINZ XE Team

s7-p3: <p>A medida que los entusiastas del motor se aficionen a estas carreras, los fabricantes de automóviles evaluarán el rendimiento de estos coches de alta tecnología, aplicando lo aprendido a la próxima generación de vehículos eléctricos.</p>


## Sección 8 - conclusion - SPANISH

s8-title: De cara al futuro

s8-p: <p>Esto llevará los coches eléctricos a cotas con los que los pioneros de los vehículos a batería nunca hubieran soñado; aumentará la demanda de un medio de transporte limpio al que por fin le ha llegado su momento.</p><p>Se prevé que las ventas de VE crecerán un 29% anual durante la próxima década, superando los 31 millones para 2030. Para 2040, más de la mitad de todos los turismos vendidos serán eléctricos.</p>

s8-counter: 50% de las ventas serán de VE

[]



    :ignore - Code ignore


***You can add comments / notes here***

Build doc: https://docs.google.com/document/d/1yIWKYX8l4f8dzRV1k4AffzsVxKBkYgvWfrWhjLanZ1E/edit

Translation doc: [https://docs.google.com/spreadsheets/d/1Mjc8WbXfCCdzCLFYDiXH8cDMBqcRPYzLaF5z03AUUo4/edit](https://docs.google.com/spreadsheets/d/1Mjc8WbXfCCdzCLFYDiXH8cDMBqcRPYzLaF5z03AUUo4/edit)


Questions:
Are we linking any of the source???
Meta data?

~~Mobile - change mobile title back to ‘fully electric’ (you changed it when showing me Paper :))~~ **Done :)**
~~Mobile - Tesla says 13% rather than 16% (car makers in the fast lane graphic)~~ 
~~mobile  - SAIC and Renault are 7% and 9% on mobile - not sure what is correct here~~ 
~~~~~~mobile - Electric odyssey: I wonder if it should auto expand on mobile on scroll or we have some sort of prompt to select a destination? I think the expanding sections could be easly missed.~~ **OK + added**
~~Section 3 chart source data, my bad;~~ **OK**

![](https://paper-attachments.dropbox.com/s_E22B9B133EEF114A08728A6121142B2D02AF9F6107E526D707A11A4933E663B5_1619801898052_image.png)


TO DO 

- https://www.bugherd.com - next week Tuesday (14 days trial for free)
- ~~safari line grow animatation backward~~
- ~~remove menu label when scrolling to an other section~~
- 

Share image

![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1620926905500_santander-ev_share-image2x.jpg)

~~~~

![](https://paper-attachments.dropbox.com/s_9BA300F816F72FF062FF19A416811953F1FDDEACE308BE77E8D968438F231AE7_1620926902171_santander-ev_share-image.jpg)


