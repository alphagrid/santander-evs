//  Link media
require.context('../media/', true, /^\.\/.*\.*/);
require.context('../fonts/', true, /^\.\/.*\.*/);


//  Styles
import '../styles/styles.scss';

// JS
import $ from 'jquery';

import utils from './proj/utils';
import InView from './proj/inview';
import { CountUp } from './proj/CountUp.min.js';
// import { TweenLite, TweenMax } from 'gsap';
// import ScrollMagic from 'scrollmagic';

let Site;
var pageContainer = '#ig-infographic';
// const isMobileDevice = utils.mobileAndTabletCheck();
// const isMobile = window.innerWidth < 800 || isMobileDevice;

var controller;
var controller_swipe;
var wipeAnimation = [];
var scene = [];

(function ($) {
	'use strict';

	Site = {
		/**
		 * initialize prototype
		 *
		 */
		init: function (elt) {
			var self = this;


			setTimeout(function(){
				self.inviewAnimation();
				self.animate_stats();
			}, 500);
			
			
			self.togglePieYear();
			self.mobileMapPinContent();
			self.raceOpenClose();
			self.wipeSections();

			// init controller for Menu + Image Sequence
			controller = new ScrollMagic.Controller();
			self.imageSequenceSections();
			self.currentMenu();
		
			$(pageContainer).addClass('page-ready');

			const resizeHandler = function() {
				self.wipeSections();
			};

			$(window).on('resize', utils.debounce(resizeHandler, 100) );

			// var pymChild = new pym.Child();
			// pymChild.sendHeight();

			if( location.hostname.match('alphagridinfographics') || location.hostname.match('localhost') ) {
				// outside of the hub as a standolone page
				self.scrollToAnchor();
			}
			else {
				// embeded into the hub
				$(pageContainer).addClass('ft-embedded');

				// stop the left menu to scroll past the end of the interactive 
				if($('.related-articles').length > 0){
					$(pageContainer).after('<div id="trigger-end"></div>')
					new ScrollMagic.Scene({ triggerElement: "#trigger-end", triggerHook: "onEnter", duration: $("#ig-infographic").height() })
						.setClassToggle("#navigation", "abs")
						// .addIndicators({name: "main"}) // add indicators (requires plugin)
						.addTo(controller);
				}

			}
			
		},

		/**
		 * Current menu item
		 *
		 */
		currentMenu: function () {
			

			$('#ig-infographic .section').each(function(){
				var trigger = $(this).attr('data-section');

				var scene = new ScrollMagic.Scene({triggerElement: "#s"+trigger, triggerHook: "onEnter"})
						.on("enter", function () {
							$('.menu a').removeClass('is-current');
							$('#menu-s'+trigger).addClass('is-current');
							$('.menu a:focus').blur();
						})
						.on("leave", function () {
							// reset style
							$('.menu a').removeClass('is-current');
							$('#menu-s'+(parseInt(trigger)-1)).addClass('is-current');
							$('.menu a:focus').blur();
						})
						// .addIndicators({name: "2 - change inline style"}) // add indicators (requires plugin)
						.addTo(controller);

			});

		},

		/**
		 * Race open close
		 *
		 */
		raceOpenClose: function () {
			
			$('.js-open-prompt').on('click', function(){
				$(this).parents('li').toggleClass('is-open');
			});

		},

		/**
		 * image sequence
		 *
		 */
		imageSequenceSections: function () {
			var images = [];

			const $ref = $('#battery-img-sequence');
			const frameCount = $ref.attr('data-frameCount');
			const extension = $ref.attr('data-extension');
			const path = $ref.attr('data-path');

			for (let i = 1; i <= frameCount; i++) {
				images.push(path+i.toString().padStart(3, '0')+extension);
			};



			// function preloadImages(array) {
			// 	if (!preloadImages.list) {
			// 		preloadImages.list = [];
			// 	}
			// 	var list = preloadImages.list;
			// 	for (var i = 0; i < array.length; i++) {
			// 		var img = new Image();
			// 		img.onload = function() {
			// 			var index = list.indexOf(this);
			// 			if (index !== -1) {
			// 				// remove image from the array once it's loaded
			// 				// for memory consumption reasons
			// 				list.splice(index, 1);
			// 			}
			// 		}
			// 		list.push(img);
			// 		img.src = array[i];
			// 	}
			// }

			// preload all images
			// preloadImages(images);

			// $.each(images, function(index, value) { 
			// 	var preload = new Image(); 
			// 	// Changing the src requests the corresponding file 
			// 	preload.src = value;     
			// });


			const preloadImages = () => {
			  for (let i = 1; i <= images.length - 1; i++) {
			    const img = new Image();
			    img.src = images[i];
			  }
			};

			// https://codepen.io/j-v-w/pen/ZEbGzyv

			// TweenMax can tween any property of any object. We use this object to cycle through the array
			var obj = {curImg: 0};


			// create tween
			const html = document.documentElement;
			const canvas = document.getElementById("battery-sequence");
			const context = canvas.getContext("2d");
			
			canvas.width=1920;
			canvas.height=1080;

			const img = new Image();
			img.src = images[obj.curImg];
			img.onload=function(){
				context.drawImage(img, 0, 0, canvas.width, canvas.height);
			}	

			const updateImage = index => {
			  img.src = index;
			  context.drawImage(img, 0, 0, canvas.width, canvas.height);

			}


			

			var tween = TweenMax.to(obj, 0.5,
				{
					curImg: images.length - 1,	// animate propery curImg to number of images
					roundProps: "curImg",				// only integers so it can be used as an array index
					repeat: 0,									// repeat 3 times
					immediateRender: true,			// load first image automatically
					ease: Linear.easeNone,			// show every image the same ammount of time
					onUpdate: function () {
					  // $("#battery-img-sequence").attr("src", images[obj.curImg]); // set the image source
					  updateImage(images[obj.curImg]);
					}
				}
			);

			preloadImages();

			// build scene to pin
			var scene_pin = new ScrollMagic.Scene({triggerElement: "#img-sequence",
							triggerHook: "onLeave",
							duration: "300%"
						})
						.setPin("#img-sequence")
						// .addIndicators() // add indicators (requires plugin)
						.addTo(controller);


			// update image from image seq list
			var scene = new ScrollMagic.Scene({triggerElement: "#trigger", 
							triggerHook: "onLeave", 
							duration: "300%"
						})
						.setTween(tween)
						// .addIndicators() // add indicators (requires plugin)
						.addTo(controller);


			new ScrollMagic.Scene({triggerElement: "#trigger", triggerHook: "onLeave", duration: "55%"})
				.setClassToggle("#img-sequence", "show1")
				// .addIndicators({name: "show1"}) // add indicators (requires plugin)
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: "#trigger", triggerHook: "onLeave", duration: "55%", offset: "500%"})
				.setClassToggle("#img-sequence", "show2")
				// .addIndicators({name: "show2"}) // add indicators (requires plugin)
				.addTo(controller);

			new ScrollMagic.Scene({triggerElement: "#trigger", triggerHook: "onLeave", duration: "55%", offset: "1000%"})
				.setClassToggle("#img-sequence", "show3")
				// .addIndicators({name: "show3"}) // add indicators (requires plugin)
				.addTo(controller);;

			new ScrollMagic.Scene({triggerElement: "#trigger", triggerHook: "onLeave", duration: "55%", offset: "1500%"})
				.setClassToggle("#img-sequence", "show4")
				// .addIndicators({name: "show4"}) // add indicators (requires plugin)
				.addTo(controller);;

			new ScrollMagic.Scene({triggerElement: "#trigger", triggerHook: "onLeave", duration: "55%", offset: "2000%"})
				.setClassToggle("#img-sequence", "show5")
				// .addIndicators({name: "show5"}) // add indicators (requires plugin)
				.addTo(controller);
		},
		/**
		 * Wipe Sections, horizontal scroll on desktop with scroll nagic
		 *
		 */
		wipeSections: function () {

			if($('body').width() >= 992) {

				if(controller_swipe) {
					// all set already
				}
				else {

					// SECTION 1
					controller_swipe = new ScrollMagic.Controller();

					// // define movement of panels
					wipeAnimation['s1'] = new TimelineLite()
						.fromTo(".wipe-s1", 1, {x: "0"}, {x: "-50%", ease: Linear.easeNone});  // in from left

					// create scene to pin and link animation
					scene['s1'] = new ScrollMagic.Scene({
							triggerElement: "#pinContainer_s1",
							triggerHook: "onLeave",
							duration: "100%"
						})
						.setPin("#pinContainer_s1")
						.setTween(wipeAnimation['s1'])
						// .addIndicators() // add indicators (requires plugin)
						.addTo(controller_swipe);




					// SECTION 3

					// // define movement of panels
					wipeAnimation['s3'] = new TimelineLite()
						.fromTo(".wipe-s3", 1, {x: "0"}, {x: "-50%", ease: Linear.easeNone});  // in from left

					// create scene to pin and link animation
					scene['s3'] = new ScrollMagic.Scene({
							triggerElement: "#pinContainer_s3",
							triggerHook: "onLeave",
							duration: "100%"
						})
						.setPin("#pinContainer_s3")
						.setTween(wipeAnimation['s3'])
						// .addIndicators() // add indicators (requires plugin)
						.addTo(controller_swipe);



					// SECTION 5

					// // define movement of panels
					wipeAnimation['s5'] = new TimelineLite()
						.fromTo(".wipe-s5", 1, {x: "0"}, {x: "-50%", ease: Linear.easeNone});  // in from left

					// create scene to pin and link animation
					scene['s5'] = new ScrollMagic.Scene({
							triggerElement: "#pinContainer_s5",
							triggerHook: "onLeave",
							duration: "100%"
						})
						.setPin("#pinContainer_s5")
						.setTween(wipeAnimation['s5'])
						// .addIndicators() // add indicators (requires plugin)
						.addTo(controller_swipe);
				}

			}
			else {
				// destroy 
				if(controller_swipe !== null && controller_swipe !== undefined) {	
					// if not undefined
					
					controller_swipe.destroy('true');
					scene['s1'].destroy('true');
					scene['s3'].destroy('true');
					scene['s5'].destroy('true');
				}
				
				controller_swipe = null;

				scene['s1'] = null;
				scene['s3'] = null;
				scene['s5'] = null;

			}
		},

		/**
		 * Scroll to
		 *
		 */
		scrollToAnchor: function () {
			// const ft_nav_height = $('#ft-fixed-nav-height').height();

			$('body').on('click', '.scrollTo', function(event){
				event.preventDefault();
				event.stopPropagation();

				var tag = $(this).attr('href');

				$('html,body').animate({scrollTop: $(tag).offset().top },'slow');
			});
		},

		/**
		 * Toggle show year svg pie
		 *
		 */
		mobileMapPinContent: function(){

			$('.js-pin-content-mobile .close').on('click', function(e){
				$('.js-pin-content-mobile').removeClass('is-open');
				$('.js-map-pins .pulse').removeClass('is-active');
			});

			$('.js-map-pins .pulse').on('click', function(e){
				var $this = $(this);

				// remove previous content
				$('.js-map-pin-content-mobile').html('');
				$('.js-map-pins .pulse').removeClass('is-active');
				$('.js-pin-content-mobile').addClass('is-open');
				
				$this.addClass('is-active');

				// get current country copy
				var copy = $this.find('.pin-content').html();
				$('.js-map-pin-content-mobile').html(copy);


			});
		},

		/**
		 * Toggle show year svg pie
		 *
		 */
		togglePieYear: function(){
			$('.js-toggle-pie-year button').on('click', function(){
				var $this = $(this);

				// button change selected state
				$('.js-toggle-pie-year button, .svg-pie').removeClass('is-selected');
				$this.addClass('is-selected');
				$($this.attr('data-show')).addClass('is-selected');
			});
		},

		/**
		 * Toggle show more / show less
		 *
		 */
		toggleExpendableContent: function(){
			$('.js-toggleExpend').on('click', function(){
				$(this).parent('.cp-expendable').toggleClass('is-open');
			});
		},


		/**
		 * inview animation
		 *
		 */
		 inviewAnimation: function(){
			$("[class*='ivanim-']").on('inview', function(event, isInView) {
				if (isInView) {
					if (!$(this).hasClass('is-inview')) {
						$(this).addClass('is-inview');
					}
				}
			});

			// inview repeat
			$("[class*='ivanimr-']").on('inview', function(event, isInView) {
				if (isInView) {
					if (!$(this).hasClass('is-inview')) {
						$(this).addClass('is-inview');
					}
				}
				else {
					if ($(this).hasClass('is-inview')) {
						$(this).removeClass('is-inview');
					}
				}
			});
		},

		/**
		 * animation on the stats
		 *
		 */
		animate_stats: function (){
			
			// add unique ID to all stats
			var stat_n = 0;
			$('.js-stat-number').each(function(){
				stat_n++;
				$(this).attr('id', 'stat'+stat_n);
			});

			// count up
			$('.js-stat-number').on('inview', function(event, isInView) {
				if (isInView) {
					if (!$(this).hasClass('is-inview')) {
						var $target = $(this);

						$target.addClass('is-inview');

						const id_val = $target.attr('id');
						const number_val = $target.attr('data-number');
						const separator_val = $target.attr('data-separator');
						const prefix_val = $target.attr('data-prefix');
						const suffix_val = $target.attr('data-suffix');
						const decimal_val = $target.attr('data-decimal');
						const duration_val = $target.attr('data-duration');

						const options = {
							prefix: prefix_val, 
							suffix: suffix_val,
							startVal: 0,
							useEasing: false, 
							useGrouping: true, 
							separator: separator_val, 
							decimal: '.', 
							duration: duration_val,
							startVal: $target.text(),
						};
						// let countUpY1 = new CountUp('year1', 2019, optionsY1);
						// countUpY1.start(methodToCallOnComplete);


						const demo = new CountUp(id_val, number_val, options);
						if (!demo.error) {
							demo.start();
						}
					}
				}
			});
		},

	};


	$(document).ready( function(){
		Site.init();
	})

})(jQuery);
