
    //  Const
    //  @description:   Define static values for use project-wide

    export const EVENTS =
    {
        READY : "ready",
        BUILD : "build",
        ERROR : "error"
    };

    export const TIME =
    {
        MINUTE : 60000,
        HOUR : 3600000,
        DAY : 86400000,
        WEEK : 604800000
    };
