import Waypoint from 'Waypoint';
import $ from 'jquery';

export default function(waypoints, waypointContext) {

	return function(idx, el) {

		var $el = $(el);

		// enter (scrolling down)
		waypoints.push(
			new Waypoint({
				element: el,
				handler: function(direction) {
					if( direction === 'down' ) {
						$el.removeClass('sticky--top sticky--bottom').addClass('active sticky--stuck');
					} else {
						$el.removeClass('active sticky--stuck sticky--bottom').addClass('sticky--top');
					}
				},
				context: waypointContext,
				offset: '52'
			})
		);

		// exit (scrolling down)
		waypoints.push(
			new Waypoint({
				element: el,
				handler: function(direction) {
					if( direction === 'down' ) {
						$el.removeClass('active sticky--stuck sticky--top').addClass('sticky--bottom');
					} else {
						$el.removeClass('sticky--top sticky--bottom').addClass('active sticky--stuck');
					}
				},
				context: waypointContext,
				offset: function() { return -this.element.clientHeight + window.innerHeight; },
			})
		);


	}

};
