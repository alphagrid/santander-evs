
import Waypoint from 'Waypoint';
import $ from 'jquery';

export default function(waypoints, waypointContext) {


	return function(idx, el) {

		const $el = $(el);
		const $tgt = $('.' + $el.data('target') );
		const clazz = $el.data('class');
		const $tgtRemove = $('.' + $el.data('target-remove') );
		const clazzRemove = $el.data('class-remove');
		const offset = $el.data('offset') || 100;

		// console.log( $tgt, clazz, $tgtRemove, clazzRemove, offset );


		waypoints.push(
			new Waypoint({
				element: el,
				handler: function(direction) {

					if( direction === 'down' ) {
						$tgt.addClass( clazz );
						$tgtRemove.removeClass( clazzRemove );
					} else {
						$tgtRemove.addClass( clazzRemove );
						$tgt.removeClass( clazz );
					}

					if( $el.hasClass('js-trigger-resize') ) {
						// console.log('should be triggering resize');
						$(window).trigger('resize');
					}

				},
				context: waypointContext,
				offset: offset + '%'
			})
		);

	}

};
