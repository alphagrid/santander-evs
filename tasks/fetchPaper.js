require('dotenv').config();


const R = require('ramda');
const fs = require('fs');

const pkg = require('../package.json');

const archieml = require('archieml')
const htmlparser = require('htmlparser2')
// const { AllHtmlEntities } = require('html-entities')
const Dropbox = require('dropbox').Dropbox;
const marked = require('marked');
const fetch = require('node-fetch');


const fileId = pkg.dataFetch.paper.doc.id;
const accessToken = process.env.DROPBOX_ACCESS_TOKEN;

const renderer = new marked.Renderer();

// renderer.text = (txt) => `_${txt}_`;


marked.setOptions({
    renderer: renderer,
    pedantic: false,
    gfm: true,
    tables: true,
    breaks: false,
    sanitize: false,
    smartLists: true,
    smartypants: false,
    xhtml: false
});



// dropbox bug
const removeDropboxThinSpace  = R.replace(' ', ' ');

const removeImages = R.replace(/^\!\[+(.*?)+\)/gm, '');
const removeNewLine = R.replace(/\n/g, '');
const formatRawDropbox = R.compose( removeImages, removeDropboxThinSpace );
const removeBoldMarkdown = R.replace(/\**/g, '');

// do this with a regex :s
const removeParaTags = R.compose( R.join(''), R.split('</p>'), R.join(''), R.split('<p>') );



// how to handle specific types
const storyElementTransforms = {
  'text' : R.compose( removeNewLine, marked ),
  'h3' : removeBoldMarkdown
};

const getStoryElementTransformation = storyEl => {
  const type = R.prop('type', storyEl);
  const func =  R.propOr( R.identity, type, storyElementTransforms );
  return func;
};



// handle the individual story element types
const convertStoryElement = ( storyEl ) => {
  const func = getStoryElementTransformation( storyEl );
  return R.evolve({
    value: func
  }, storyEl)

}




// apply any transformations to the document as a whole
const documentTransformation = {
  byline: R.compose( removeParaTags, removeNewLine, marked ),
  byline2: R.compose( removeParaTags, removeNewLine, marked ),
  story: R.map( convertStoryElement )
}



function parseHTML(doc) {


  fs.writeFileSync('./src/data/story.md', Buffer.from(doc.fileBinary, 'binary').toString('utf8'), { flag: 'w+' } );

  const docHTML = Buffer.from(doc.fileBinary, 'binary').toString('utf8')
    .replace(/span>\s<span/g, 'span><span> </span><span') // paper hack

  const data = archieml.load( formatRawDropbox(docHTML) );

  const result = R.evolve( documentTransformation )( data );

  // console.log( result );

  // const  elements = R.map( )
  const dataObj = JSON.stringify(result, null, 2);
  console.log( dataObj );


  fs.writeFileSync('./src/data/data.json', dataObj, { flag: 'w+' } );

  return data;
}



function getDoc (docId, accessToken, format = 'markdown') {
  const dbx = new Dropbox({ accessToken, fetch })
  return dbx.paperDocsDownload({ doc_id: docId, export_format: format })
}



getDoc(fileId, accessToken)
  .then( parseHTML );
